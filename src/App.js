import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import './App.css';
import Presentation from './Presentation';

const { Header, Content, Footer } = Layout;

class App extends Component {
  render() {
    return (
      <Layout className="layout">
          <Header>
            <div className="logo" />
            <Menu
              theme="dark"
              mode="horizontal"
              defaultSelectedKeys={['2']}
              style={{ lineHeight: '64px' }}
            >
              <Menu.Item key="1">Presentations</Menu.Item>
            </Menu>
          </Header>
          <Content style={{ padding: '0 50px' }}>
            <Presentation style={{ background: '#fff', padding: 24, minHeight: 280 }}/>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Created with React & Antd, by Daniel Sun
          </Footer>
        </Layout>
    );
  }
}

export default App;
