import React, { Component } from 'react';
import { Icon } from 'antd';
import $ from 'jquery'

class Presentation extends Component {
  constructor(props) {
    super(props);
    this.state = {items: [
      {
          "id": 1,
          "title": "Introduction to Hydrawise Controllers",
          "type": "text",
          "content": "Hydrawise Controllers are internet the best.",
          "transition": "fade"
      },
      {
          "id": "3",
          "content": "http://www.irrigationstore.com.au/Library/hc_ind1.jpg",
          "type": "image",
          "transition": "slide"
      },
      {
          "id": 2,
          "title": "Take control of your irrigation at any time,\\n from anywhere",
          "type": "text",
          "content": "Reduce costly service calls with remote access to control your irrigation system from your phone, tablet, or computer at any time from wherever you are."
      },
      {
          "id": 6,
          "title": "Easy to use",
          "type": "txt",
          "content": "Simple and straight-forward installation with step-by-step setup wizard."
      },
      {
          "id": 7,
          "title": "Save water",
          "transition": "fade",
          "content": "Uses weather station information and localized forecasts to predict, change, monitor, measure,and report on your irrigation."
      }
    ]};
  }
  componentWillMount() {
    this.setState({
      sortedItems: this.state.items.sort((a, b) => a.id - b.id).slice(),
      currentSlide: 0
    });
  }
  componentDidMount() {
    $(".carousel-container").slidesjs({
        width: 440,
        height: 500,
        navigation: {
          active: false,
          effect: "fade"
        }
      });
  }
  render() {
    return (
      <div className="row prensentation">
        <div className="col-md-4 col-sm-offset-4">
          <div className="carousel-container">
            {this.state.sortedItems.map((item,index) => (
              <SingleSlide key={item.id} item={item} />
            ))}
            <a href="#" className="slidesjs-previous slidesjs-navigation"><Icon type="left" /></a>
            <a href="#" className="slidesjs-next slidesjs-navigation"><Icon type="right" /></a>
          </div>
        </div>
      </div>
    );
  }
}

class SingleSlide extends Component {
  render() {
    var transitionClass;
    if (this.props.item.transition===null || this.props.item==='') {
      transitionClass='transition-slide';
    }
    else {
      transitionClass='transition-'+this.props.item.transition;
    }
    if (this.props.item.type==="image") {
      var divStyle = {backgroundImage: 'url(' + this.props.item.content + ')'}
      return (
        <div className={transitionClass+' single-slide slide-image'} style={divStyle}>
        </div>
      );
    }
    else {
      return (
        <div className={transitionClass+' single-slide slide-text'}>
          <h2>{this.props.item.title}</h2>
          <p>{this.props.item.content}</p>
        </div>
      );
    }
  }
}

export default Presentation;
