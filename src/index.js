import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './Resources/jquery.slides.js';

import './index.css';
import './Resources/slide.css';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
